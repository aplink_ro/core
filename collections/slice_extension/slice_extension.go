package slice_extension

func ContainsString(item string, arr []string) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}

func ContainsInt(item int, arr []int) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}

func ContainsInt32(item int32, arr []int32) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}

func ContainsInt64(item int64, arr []int64) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}

func ContainsFloat64(item float64, arr []float64) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}

func ContainsFloat32(item float32, arr []float32) bool {
	for _, val := range arr {
		if item == val {
			return true
		}
	}
	return false
}
