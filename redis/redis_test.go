package redis

import (
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"testing"
	"time"
)

func TestRedis(t *testing.T) {
	log.Init("debug", "")
	Init("127.0.0.1", "6379", 0, 50, 100)
	err := RedisDB.Client.Set("test", "123", 30*time.Minute).Err()
	if err != nil {
		fmt.Println(err)
	}
}

func TestRedisLock_Lock(t *testing.T) {
	Init("127.0.0.1", "6379", 0, 50, 100)
	lockKey := "test:lock:key"

	fmt.Println("lock2")
	lock2 := NewRedisLock(lockKey + "2")

	err1 := lock2.Lock(RedisDB.Client, time.Second*60)
	if err1 != nil {
		fmt.Println(err1)
	}

	lock2.Unlock(RedisDB.Client)
	//lock2.Unlock(RedisDB.Client)
	result, err := RedisDB.Client.Get(lockKey + "2").Result()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(result)

	time.Sleep(10 * time.Second)

	flag := lock2.Unlock(RedisDB.Client)
	fmt.Println(flag)

	result, err = RedisDB.Client.Get(lockKey + "2").Result()
	if err != nil {
		fmt.Println("11" + err.Error())
		fmt.Println("11" + result)
		return
	}
	fmt.Println(result)

}
