package redis

import (
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
)

type redisHelper struct {
	Client *redis.Client
}

// RedisDB redisdb instance
var RedisDB *redisHelper

// InitRedisDB initial redisDB
func Init(host, port string, db, poolSize, minIdle int) {
	client := redis.NewClient(&redis.Options{
		Addr:         fmt.Sprintf("%s:%s", host, port),
		MaxRetries:   30,
		DB:           db,
		PoolSize:     poolSize,
		MinIdleConns: minIdle,
	})

	RedisDB = &redisHelper{Client: client}
	if err := RedisDB.Client.Ping().Err(); err != nil {
		log.Logger.Errorf("redis init failed", err)
	} else {
		log.Logger.Info("redis init success")
	}

}
