package redis

import (
	"errors"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"time"
)

type RedisLock struct {
	LockKey string
	Value   string
}

var delScript = redis.NewScript(`
if redis.call("get", KEYS[1]) == ARGV[1] then
	return redis.call("del", KEYS[1])
else
	return 0
end`)

func NewRedisLock(lockKey string) *RedisLock {
	return &RedisLock{
		LockKey: lockKey,
		Value:   uuid.New().String(),
	}
}

func (this *RedisLock) Lock(client *redis.Client, timeout time.Duration) error {

	lockReply, err := client.SetNX(this.LockKey, this.Value, timeout).Result()
	if err != nil {
		lockReply = false
	}
	if lockReply {
		return nil
	} else {
		log.Logger.Infof("redis lock fail error %+v lockkey %+v VALUE %+v", err, this.LockKey, this.Value)
		return errors.New("lock fail")
	}
}

func (this *RedisLock) Unlock(client *redis.Client) bool {
	v, err := delScript.Run(client, []string{this.LockKey}, this.Value).Result()
	if v == 0 || err != nil {
		return false
	}
	return true
}
