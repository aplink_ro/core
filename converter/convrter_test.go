package converter

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

type ColorType int32

const (
	Yellow ColorType = 0
	Red    ColorType = 1
	Green  ColorType = 2
)

type Pagination struct {
	TotalRecord int32 `json:"totalRecord"`
	CurrentPage int32 `json:"currentPage"`
	PageSize    int32 `json:"pageSize"`
	TotalPages  int32 `json:"totalPages"`
	HasNext     bool  `json:"hasNext"`
}

//table_index tag 目前没有使用。
type Apple struct {
	Color     ColorType `json:"color" table_header:"颜色" table_index:"1" table_enum:"color_map"`
	Style     string    `json:"style" table_header:"风格" row_span_count:"6" table_index:"3"`
	Big       float64   `json:"big" table_header:"大小" table_index:"2"`
	Comment   string    `json:"-" table_header:"备注" table_index:"4"`
	Timestamp int64     `json:"timestamp" table_header:"时间" table_index:"5" table_content_type:"timestamp"`
}

func TestListToFormatTable(t *testing.T) {

	enumsMap := map[string]map[string]string{}

	enumsMap["color_map"] = map[string]string{
		"Yellow": "0",
		"Red":    "1",
		"Green":  "2",
	}

	apple := Apple{
		Color:     Yellow,
		Style:     "shandong",
		Big:       1.123456789876544,
		Comment:   "1",
		Timestamp: 1537708218726,
	}
	list := []interface{}{}

	list = append(list, apple)
	apple = Apple{
		Color:     Red,
		Style:     "shandong",
		Big:       0.0000000000000001,
		Comment:   "2",
		Timestamp: 1537708218726,
	}
	list = append(list, apple)

	tables := ListToFormatTable(list, enumsMap)
	j, _ := json.Marshal(tables)
	fmt.Println(string(j))
}

func TestStringFormatMSToTime(t *testing.T) {
	//tm, err := StringFormatSecondToTIme("2020-08-05 09:33:10.0")
	//if err != nil {
	//	t.Log(err)
	//} else {
	//	t.Log(tm)
	//}

	tt, err := time.Parse(time.UnixDate, "Wed Aug 05 11:10:05 UTC 2020")
	t.Log(tt)
	if err != nil {
		t.Log(err)
	}
}
