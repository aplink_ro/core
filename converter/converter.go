package converter

import (
	"fmt"
	"github.com/goinggo/mapstructure"
	"gitlab.com/mini-tech/aptoken/aplink-core/basic/model"
	"reflect"
	"strconv"
	"time"
)

func StructToMap(obj interface{}) map[string]interface{} {
	obj1 := reflect.TypeOf(obj)
	obj2 := reflect.ValueOf(obj)

	var data = make(map[string]interface{})
	for i := 0; i < obj1.NumField(); i++ {
		data[obj1.Field(i).Name] = obj2.Field(i).Interface()
	}
	return data
}

func MapToStruct(source map[string]interface{}, dest interface{}) error {
	err := mapstructure.Decode(source, &dest)
	return err
}

func Int64ToString(value int64) string {
	return strconv.FormatInt(value, 10)
}

func IntToString(value int) string {
	return strconv.Itoa(value)
}

func StringToInt64(value string) (int64, error) {
	return strconv.ParseInt(value, 10, 64)
}

func StringToInt(value string) (int, error) {
	return strconv.Atoi(value)
}

func StringFormatSecondToTIme(value string) (time.Time, error) {
	return time.Parse("2006-01-02 15:04:05", value)
}

func ListToFormatTable(data []interface{}, enumMap map[string]map[string]string) *model.FormatTable {
	tables := model.FormatTable{
		Headers:  []model.FormatTableHeader{},
		Contents: [][]interface{}{},
	}

	if len(data) == 0 {
		return &tables
	}

	elements := reflect.ValueOf(data)
	headerKey := make(map[string]struct{})
	for i := 0; i < elements.Len(); i++ {
		t := reflect.TypeOf(elements.Index(i).Interface())
		v := reflect.ValueOf(elements.Index(i).Interface())
		contentItems := []interface{}{}
		for j := 0; j < t.NumField(); j++ {
			if jsonTag, ok := t.Field(j).Tag.Lookup("json"); ok {
				if jsonTag == "-" || len(jsonTag) == 0 {
					continue
				}
			}
			header := model.FormatTableHeader{
				Name:         t.Field(j).Tag.Get("json"),
				Title:        t.Field(j).Tag.Get("table_header"),
				Type:         t.Field(j).Tag.Get("table_content_type"),
				RowSpanCount: t.Field(j).Tag.Get("row_span_count"),
			}
			if enumMap != nil && len(enumMap) > 0 {
				if enumKey, ok := t.Field(j).Tag.Lookup("table_enum"); ok {
					if enumValue, ok := enumMap[enumKey]; ok {
						header.EnumsMap = enumValue
					}
				}
			}
			if _, ok := headerKey[header.Name]; !ok {
				tables.Headers = append(tables.Headers, header)
				headerKey[header.Name] = struct{}{}
			}

			contentItems = append(contentItems, fmt.Sprintf("%v", v.Field(j)))
		}
		tables.Contents = append(tables.Contents, contentItems)
	}
	return &tables
}
