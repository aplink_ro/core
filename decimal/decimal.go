package decimal

import (
	"fmt"
	"github.com/shopspring/decimal"
	"strings"
)

func Truncate(f float64, pre int32) float64 {
	result, _ := decimal.NewFromFloat(f).Truncate(pre).Float64()
	return result
}

func TruncateToString(f float64, pre int32) string {
	format := "%." + fmt.Sprintf("%df", pre)
	return fmt.Sprintf(format, Truncate(f, 8))
}

func NewFloat64FromStr(f string, pre int32) (float64, error) {
	d, err := decimal.NewFromString(f)
	if err != nil {
		return -1, err
	}
	r, _ := d.Truncate(pre).Float64()
	return r, nil
}

func SumList(values []float64) float64 {
	d := decimal.Decimal{}
	for _, v := range values {
		d = d.Add(decimal.NewFromFloat(v))
	}
	f, _ := d.Float64()
	return f
}

func Sub(first float64, second float64) float64 {
	f, _ := decimal.NewFromFloat(first).Sub(decimal.NewFromFloat(second)).Float64()
	return f
}

//float64求和(不丢失精度)
func Sum(first float64, rest ...float64) (f float64, exact bool) {
	desc := []decimal.Decimal{}
	for _, v := range rest {
		desc = append(desc, decimal.NewFromFloat(v))
	}
	return decimal.Sum(decimal.NewFromFloat(first), desc...).Float64()
}

func RoundUp(d decimal.Decimal, precision int32) decimal.Decimal {
	if d.Round(precision).Equal(d) {
		return d
	}

	halfPrecision := decimal.New(5, -precision-1)

	return d.Add(halfPrecision).Round(precision)
}

// 向下截取
func RoundDown(d decimal.Decimal, precision int32) decimal.Decimal {
	if d.Round(precision).Equal(d) {
		return d
	}

	halfPrecision := decimal.New(5, -precision-1)

	return d.Sub(halfPrecision).Round(precision)
}

// 判断小数的精度是不是指定精度
func IsPrecision(d decimal.Decimal, precision int32) bool {
	splitAmount := strings.Split(d.String(), `.`)
	if len(splitAmount) <= 1 {
		return precision == 0
	} else {
		return len(splitAmount[1]) == int(precision)
	}
}

func GetPrecision(d decimal.Decimal) int32 {
	splitAmount := strings.Split(d.String(), `.`)
	if len(splitAmount) <= 1 {
		return 0
	} else {
		return int32(len(splitAmount[1]))
	}
}
