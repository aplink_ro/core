package decimal

import (
	"fmt"
	"github.com/shopspring/decimal"
	"testing"
)

func TestRoundUp(t *testing.T) {
	d := decimal.NewFromFloat(1.239)
	x1 := d.Round(2)
	x := RoundUp(d, 2)
	x2 := RoundDown(d, 2)
	fmt.Println(x)
	fmt.Println(x1)
	fmt.Println(x2)
	fmt.Println(d.Truncate(2))
}
