package mq

import (
	"errors"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"sync"
	"time"
)

// kafka 生产者
type KafkaProducer struct {
	brokers       []string             // kafka 集群 brokers
	asyncProducer sarama.AsyncProducer // 异步生产者
	syncProducer  sarama.SyncProducer  // 同步生产者
	stopWg        sync.WaitGroup       // 停止时等待协程完成
}

// 关闭连接
func (p *KafkaProducer) Close() error {
	if p.asyncProducer != nil {
		p.stopWg.Wait() // 等待协程完成
		if err := p.asyncProducer.Close(); err != nil {
			return err
		}
		p.asyncProducer = nil
	}
	if p.syncProducer != nil {
		p.syncProducer.Close()
	}
	return nil
}

// 建立连接
func (p *KafkaProducer) Connect(brokers []string) error {
	if p.asyncProducer != nil {
		return errors.New("kafka producer connect not nil")
	}

	p.brokers = brokers
	p.stopWg = sync.WaitGroup{}

	config := sarama.NewConfig()
	config.Producer.Retry.Max = 5
	config.Producer.RequiredAcks = sarama.WaitForLocal // Only wait for the leader to ack
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.Compression = sarama.CompressionSnappy   // Compress messages
	config.Producer.Flush.Frequency = 500 * time.Millisecond // Flush batches every 500ms

	var err error
	p.asyncProducer, err = sarama.NewAsyncProducer(brokers, config)
	if err != nil {
		return err
	}

	p.syncProducer, err = sarama.NewSyncProducer(brokers, config)
	if err != nil {
		return err
	}

	log.ZapLogger.Info("create kafka produce connection success")
	return nil
}

// 异步发送消息
func (p *KafkaProducer) AsyncSendMessage(data []byte, topic string, retryChannel chan *sarama.ProducerMessage) error {
	if p.asyncProducer == nil {
		return errors.New("kafka produce not connect")
	}
	message := &sarama.ProducerMessage{Topic: topic,
		Value: sarama.ByteEncoder(data)}
	// 消息写入生产者连接通道中
	p.asyncProducer.Input() <- message

	p.stopWg.Add(1) // 增加 1 个协程
	go func(message *sarama.ProducerMessage, retryChannel chan *sarama.ProducerMessage) {
		defer p.stopWg.Done() // 协程完成（消息回执接收完成后才能停止）
		select {
		case res := <-p.asyncProducer.Successes():
			log.ZapLogger.Debug(fmt.Sprintf("send to kafka topic %s success partition %d offset %d", topic, res.Partition, res.Offset))
		case err := <-p.asyncProducer.Errors():
			if retryChannel != nil {
				retryChannel <- message
			}
			log.ZapLogger.Error(fmt.Sprintf("send to kafka topic %s has error %s", topic, err.Error()))

		}
	}(message, retryChannel)

	return nil
}

// 同步发送消息
func (p *KafkaProducer) SyncSendMessage(data []byte, topic string) (partition int32, offset int64, err error) {
	if p.syncProducer == nil {
		return -1, -1, errors.New("kafka produce not connect")
	}
	return p.syncProducer.SendMessage(&sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.ByteEncoder(data),
	})
}

func (p *KafkaProducer) SyncBatchSendMessages(message []*sarama.ProducerMessage) error {
	return p.syncProducer.SendMessages(message)
}
