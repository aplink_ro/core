package mq

import (
	"context"
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"go.uber.org/zap"
)

type ResetOffsetType uint

const (
	Default            ResetOffsetType = 0 //默认，从上次consumer读取的offset开始
	ResetNewestOffset  ResetOffsetType = 1 //从最新的offset开始
	ResetOldestOffset  ResetOffsetType = 2 //从最旧的offset开始
	ResetSpecialOffset ResetOffsetType = 3 //设置特殊的offset
)

type KafkaConsumerGroupConf struct {
	GroupName        string          // 消费者组名
	Brokers          []string        // broker 列表
	Topics           []string        // 订阅主题
	ResetOffsetType  ResetOffsetType // 是否重置到当前分区最新的偏移量（只消费最新消息，旧消息跳过）
	SpecialOffset    int64
	ManualMarkOffset bool // 是否手动ack message offset (默认false 为自动)
}

type MessageHandler func(topic string, value []byte) error

type KafkaConsumerGroup struct {
	client   sarama.ConsumerGroup // 消费者组客户端
	stopChan chan struct{}
}

// 关闭
func (k *KafkaConsumerGroup) Close() error {
	close(k.stopChan)
	if k.client != nil {
		err := k.client.Close()
		if err != nil {
			return err
		}
		log.ZapLogger.Info("kafka consumer group closed")
		k.client = nil
	}

	return nil
}

// 创建
func NewKafkaConsumerGroup(conf *KafkaConsumerGroupConf, handler MessageHandler) (*KafkaConsumerGroup, error) {
	if conf == nil || handler == nil {
		return nil, fmt.Errorf("conf or handler is nil")
	}

	kafkaConsumerGroup := &KafkaConsumerGroup{
		client:   nil,
		stopChan: make(chan struct{}),
	}

	version, err := sarama.ParseKafkaVersion("2.1.1") // 支持的最低版本号
	if err != nil {
		return nil, err
	}
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	config.Version = version
	if conf.ResetOffsetType == ResetOldestOffset {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}
	consumer := ConsumerHandler{
		ready:            make(chan bool),
		handler:          handler,
		resetOffsetType:  conf.ResetOffsetType,
		manualMarkOffset: conf.ManualMarkOffset,
		specialOffset:    conf.SpecialOffset,
		conf:             conf,
	}
	ctx := context.Background()
	kafkaConsumerGroup.client, err = sarama.NewConsumerGroup(conf.Brokers, conf.GroupName, config)
	if err != nil {
		return nil, err
	}

	go func() {
		for {
			select {
			case <-kafkaConsumerGroup.stopChan:
				return
			default:
				// `Consume` should be called inside an infinite loop, when a
				// server-side rebalance happens, the consumer session will need to be
				// recreated to get the new claims
				if kafkaConsumerGroup.client == nil {
					return
				}
				if err := kafkaConsumerGroup.client.Consume(ctx, conf.Topics, &consumer); err != nil {
					log.ZapLogger.Error("Consume message failed",
						zap.String("error", err.Error()),
					)
				}
				// check if context was cancelled, signaling that the consumer should stop
				if ctx.Err() != nil {
					return
				}
				consumer.ready = make(chan bool)
			}
		}
	}()

	<-consumer.ready // Await till the consumer has been set up
	log.ZapLogger.Info("Sarama consumer up and running!...")

	return kafkaConsumerGroup, nil
}

// Consumer represents a Sarama consumer group consumer
type ConsumerHandler struct {
	ready            chan bool
	handler          MessageHandler // 消息处理绑定
	resetOffsetType  ResetOffsetType
	manualMarkOffset bool
	specialOffset    int64
	session          *sarama.ConsumerGroupSession
	conf             *KafkaConsumerGroupConf
}

// Setup is run at the beginning of a new session, before ConsumeClaim
func (consumer *ConsumerHandler) Setup(session sarama.ConsumerGroupSession) error {
	if consumer.session == nil {
		consumer.session = &session
	}
	log.ZapLogger.Info("new kafka consumer group session is setup")
	// Mark the consumer as ready
	close(consumer.ready)
	claims := session.Claims()
	if consumer.resetOffsetType == Default {
		return nil
	}

	version, err := sarama.ParseKafkaVersion("2.1.1") // 支持的最低版本号
	if err != nil {
		log.Logger.Error(err)
		return err
	}
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	config.Version = version
	if consumer.conf.ResetOffsetType == ResetOldestOffset {
		config.Consumer.Offsets.Initial = sarama.OffsetOldest
	}

	client, err := sarama.NewClient(consumer.conf.Brokers, config)
	if err != nil {
		log.Logger.Error(err)
		return err
	}

	for topic, partitions := range claims {
		for _, partition := range partitions {
			var offset int64 = 0
			var err error
			if consumer.resetOffsetType == ResetNewestOffset {
				offset, err = client.GetOffset(topic, partition, sarama.OffsetNewest)
				if err != nil {
					log.Logger.Error(err)
					return err
				}
				session.MarkOffset(topic, partition, offset, "")
			} else if consumer.resetOffsetType == ResetOldestOffset {
				offset, err = client.GetOffset(topic, partition, sarama.OffsetOldest)
				if err != nil {
					log.Logger.Error(err)
					return err
				}
			} else if consumer.resetOffsetType == ResetSpecialOffset {
				session.MarkOffset(topic, partition, consumer.specialOffset, "")
				offset = consumer.specialOffset
			}
			log.ZapLogger.Info(fmt.Sprintf("reset kafka topic %s partition %d offset to %v", topic, partition, offset))

			session.ResetOffset(topic, partition, offset, "")

		}
	}

	err = client.Close()
	if err != nil {
		log.Logger.Error(err)
		return err
	}

	return nil
}

// Cleanup is run at the end of a session, once all ConsumeClaim goroutines have exited
func (consumer *ConsumerHandler) Cleanup(session sarama.ConsumerGroupSession) error {
	log.ZapLogger.Info("kafka session is close")
	return nil
}

// ConsumeClaim must start a consumer loop of ConsumerGroupClaim's Messages().
func (consumer *ConsumerHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	// NOTE:
	// Do not move the code below to a goroutine.
	// The `ConsumeClaim` itself is called within a goroutine, see:
	// https://github.com/Shopify/sarama/blob/master/consumer_group.go#L27-L29
	offset := claim.InitialOffset()
	maxOffset := claim.HighWaterMarkOffset()
	topic := claim.Topic()
	partition := claim.Partition()
	log.ZapLogger.Info(fmt.Sprintf("consum topic %s on partition %d init offset %d highwateroffset %v", topic, partition, offset, maxOffset))
	for message := range claim.Messages() {
		//log.ZapLogger.Debug(fmt.Sprintf("Message claimed: offset = %d, timestamp = %v, topic = %s", message.Offset, message.Timestamp, message.Topic))

		err := consumer.handler(message.Topic, message.Value)
		if err != nil {
			log.ZapLogger.Error(fmt.Sprintf("consumer handler has error %s", err.Error()))
			return err
		}
		if !consumer.manualMarkOffset {
			//log.Logger.Debugf("mark topic %v offset %v", message.Topic, message.Offset)
			session.MarkMessage(message, "")
		}

	}

	return nil
}

func (consumer *ConsumerHandler) MarkMessage(msg *sarama.ConsumerMessage, metadata string) {
	(*consumer.session).MarkMessage(msg, "")
}
