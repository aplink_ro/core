package mq

import (
	"fmt"
	"github.com/Shopify/sarama"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"
)

func TestKafkaConsumerGroup(t *testing.T) {
	err := log.Init("debug", "test_kafka_consumer_group")
	if err != nil {
		t.Errorf("init log has error %s", err.Error())
	}
	config := &KafkaConsumerGroupConf{
		GroupName:        "test_new_price_group_test",
		Brokers:          []string{"127.0.0.1:9092"},
		Topics:           []string{"test"},
		ResetOffsetType:  ResetNewestOffset,
		ManualMarkOffset: true,
	}

	messageHandler := func(topic string, value []byte) error {
		log.Logger.Infof("topic %s value %+v ", topic, string(value))
		return nil
	}

	kafkaConsumerGroup, err := NewKafkaConsumerGroup(config, messageHandler)

	if err != nil {
		t.Error(err)
	}
	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	select {
	case <-sigterm:
		t.Log("terminating: via signal")
		err := kafkaConsumerGroup.Close()
		if err != nil {
			t.Logf("close has error %s", err.Error())
		}
	}

	t.Log("end")
}

func TestKafkaClient(t *testing.T) {
	conf := &KafkaConsumerGroupConf{
		GroupName: "test_group_aleen_112221",
		Brokers:   []string{"127.0.0.1:9092"},
		Topics:    []string{"test"},
	}
	version, err := sarama.ParseKafkaVersion("2.1.1") // 支持的最低版本号
	if err != nil {
		t.Error(err)
	}
	config := sarama.NewConfig()
	config.Consumer.Return.Errors = true
	config.Version = version
	client, err := sarama.NewClient(conf.Brokers, config)
	if err != nil {
		t.Error(err)
	}
	oldestOffset, err := client.GetOffset(conf.Topics[0], 0, sarama.OffsetOldest)
	newestOffset, err := client.GetOffset(conf.Topics[0], 0, sarama.OffsetNewest)
	offset, err := client.GetOffset(conf.Topics[0], 0, time.Now().UnixNano()/1e6)
	fmt.Println(oldestOffset)
	fmt.Println(newestOffset)
	fmt.Println(offset)

}
