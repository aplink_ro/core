package mq

import (
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"testing"
)

func TestKafka_AsyncSendMessage(t *testing.T) {
	err := log.Init("debug", "test_kafka_consumer_group")
	if err != nil {
		t.Fatalf("init log has error %s", err.Error())
	}
	var kafkaProducer = new(KafkaProducer)
	err = kafkaProducer.Connect([]string{"127.0.0.1:9092"})
	if err != nil {
		t.Fatalf("create kafka producer has err %s", err.Error())
	}

	for i := 400; i < 500; i++ {
		err = kafkaProducer.AsyncSendMessage([]byte(fmt.Sprintf("test--%v", i)), "test", nil)
		if err != nil {
			t.Fatalf("send kafka message has err %s", err.Error())
		}
	}

	err = kafkaProducer.Close()
	if err != nil {
		t.Fatalf("close kafka produce connect has err %s", err.Error())
	}
}

func TestKafka_SyncSendMessage(t *testing.T) {
	err := log.Init("debug", "test_kafka_consumer_group")
	if err != nil {
		t.Fatalf("init log has error %s", err.Error())
	}
	var kafkaProducer = new(KafkaProducer)
	err = kafkaProducer.Connect([]string{"127.0.0.1:9092"})
	if err != nil {
		t.Fatalf("create kafka producer has err %s", err.Error())
	}

	for i := 240; i < 242; i++ {
		p, o, err := kafkaProducer.SyncSendMessage([]byte(fmt.Sprintf("test--%v", i)), "test")
		if err != nil {
			t.Fatalf("send kafka message has err %s", err.Error())
		}
		log.Logger.Infof("partition %v offset %v", p, o)
	}

	err = kafkaProducer.Close()
	if err != nil {
		t.Fatalf("close kafka produce connect has err %s", err.Error())
	}
}
