package simple_http

import (
	"fmt"
	"github.com/parnurzeal/gorequest"
	"net/http"
	"time"
)

const FORM_URLENCODED = "application/x-www-form-urlencoded"
const FORM_DATA = "multipart/form-data"
const JSON = "application/json"
const ContentTypeKey = "Content-Type"

func Get(url string, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	request := gorequest.New()
	request = request.Get(url)
	if headers != nil {
		for k, v := range headers {
			request = request.Set(k, v)
		}
	}

	if params != nil {
		for k, v := range params {
			request = request.Param(k, fmt.Sprintf("%v", v))
		}
	}

	if timeOut != nil {
		request = request.Timeout(*timeOut)
	}
	return request.EndBytes()
}

func PostFormData(url string, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	request := gorequest.New()
	request = request.Post(url)
	request = request.Set(ContentTypeKey, FORM_DATA)
	return innerPost(request, headers, params, timeOut)
}

func PostFormUrlencoded(url string, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	request := gorequest.New()
	request = request.Post(url)
	request = request.Set(ContentTypeKey, FORM_URLENCODED)
	return innerPost(request, headers, params, timeOut)
}

func PostJson(url string, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	request := gorequest.New()
	request = request.Post(url)
	request = request.Set(ContentTypeKey, JSON)
	return innerPost(request, headers, params, timeOut)
}

func Post(url string, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	request := gorequest.New()
	request = request.Post(url)

	return innerPost(request, headers, params, timeOut)
}

func innerPost(request *gorequest.SuperAgent, headers map[string]string, params map[string]interface{}, timeOut *time.Duration) (*http.Response, []byte, []error) {
	for k, v := range headers {
		request = request.Set(k, v)
	}
	request = request.SendMap(params)
	if timeOut != nil {
		request = request.Timeout(*timeOut)
	}
	return request.EndBytes()
}
