package simple_http

import (
	"fmt"
	"testing"
)

func TestClient_Get(t *testing.T) {
	params := map[string]interface{}{
		"tokens":     "BTC",
		"legalCoins": "BTC,USDT,CNY",
	}

	if resp, body, errs := Get("https://www.ibear.com/api/quote/v1/rates", nil, params, nil); len(errs) == 0 {
		if resp.StatusCode == 200 {
			fmt.Println(string(body))
		}
	}
}

func TestClient_PostFormUrlencoded(t *testing.T) {

}
