package dbank_http

import (
	"encoding/json"
	"fmt"
	"gitlab.com/mini-tech/aptoken/aplink-core/http/fast_http"
	"time"
)

type RequestClient interface {
	Post(path string, body []byte) ([]byte, error)
	BuildRequestBody(method string, data interface{}) ([]byte, error)
	AuthorizedToken(token string) RequestClient
}

type RequestBody struct {
	Version   string      `json:"version"`
	Method    string      `json:"method"`
	AppId     string      `json:"appid"`
	Timestamp string      `json:"timestamp"`
	Data      interface{} `json:"data"`
}

func (r *RequestBody) serialization() ([]byte, error) {
	fmt.Printf("%+v \n", r)
	defaultBodyByte, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	return defaultBodyByte, nil
}

type RequestConfig struct {
	Version string
	AppId   string
}

type DBankRequestClient struct {
	mux     string
	config  *RequestConfig
	Header  map[string]string
	Timeout time.Duration
}

func NewDBankHTTPClient(appID string, version string) *DBankRequestClient {
	const defaultTimeout = 10 * time.Second
	config := &RequestConfig{
		Version: version,
		AppId:   appID,
	}

	client := &DBankRequestClient{
		config:  config,
		Timeout: defaultTimeout,
		Header:  make(map[string]string),
	}

	client.Header["Content-Type"] = "application/json"
	return client
}

func (d *DBankRequestClient) AuthorizedToken(token string) RequestClient {
	d.Header["token"] = token
	return d
}

func (d *DBankRequestClient) BuildRequestBody(method string, data interface{}) ([]byte, error) {
	body, err := d.buildRequestBody(method, data).serialization()
	if err != nil {
		return nil, err
	}
	return body, nil
}

func (d *DBankRequestClient) Post(path string, body []byte) ([]byte, error) {
	httpClient := fast_http.NewHttpClient(d.Timeout)
	resp, err := httpClient.Post(path, body, d.Header)
	if err != nil {
		return nil, err
	}
	return resp.Body(), nil
}

func (d *DBankRequestClient) buildRequestBody(method string, data interface{}) *RequestBody {
	body := &RequestBody{}
	body.Method = method
	body.Version = d.config.Version
	body.AppId = d.config.AppId
	body.Data = data
	body.Timestamp = fmt.Sprintf("%d", time.Now().Unix()*1000)
	return body
}
