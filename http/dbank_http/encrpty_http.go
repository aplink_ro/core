package dbank_http

import (
	"encoding/hex"
	"encoding/json"
)



type EncryptionDBankRequestClient struct {
	encryption Encryption
	client     *DBankRequestClient
}

func NewEncryptionDBankRequestClient(client *DBankRequestClient, encryption Encryption) *EncryptionDBankRequestClient {
	if client == nil {
		return nil
	}
	return &EncryptionDBankRequestClient{
		client:     client,
		encryption: encryption,
	}
}

func (e *EncryptionDBankRequestClient) BuildRequestBody(method string, data interface{}) ([]byte, error) {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	encryptBody, err := e.encryption.Encrypt(dataBytes)
	if err != nil {
		return nil, err
	}

	body, err := e.client.buildRequestBody(method, string(encryptBody)).serialization()
	if err != nil {
		return nil, err
	}
	return body, nil
}

func (e *EncryptionDBankRequestClient) AuthorizedToken(token string) RequestClient {
	return e.client.AuthorizedToken(token)
}

func (e *EncryptionDBankRequestClient) Post(path string, body []byte) ([]byte, error) {
	encryptResponseBody, err := e.client.Post(path, body)
	if err != nil {
		return nil, err
	}
	hexBytes, err := hex.DecodeString(string(encryptResponseBody))
	if err != nil {
		return nil, err
	}
	responseBody, err := e.encryption.Decrypt(hexBytes)
	if err != nil {
		return nil, err
	}
	return responseBody, err
}
