package dbank_http

import (
	"encoding/json"
	"testing"
)

type BaseResponse struct {
	Code    string `json:"code"`
	Msg     string `json:"msg"`
	Success bool   `json:"success"`
}

type Login struct {
	Token string `json:"token"`
}

type LoginResponse struct {
	BaseResponse
	Data *Login `json:"data"`
}

type LoginRequest struct {
	PointJson   string `json:"point_json"`
	Account     string `json:"account"`
	Password    string `json:"password"`
	CountryCode string `json:"country_code"`
}

func TestEncryptPost(t *testing.T){

	loginRequest := &LoginRequest{
		PointJson:   "233",
		Account:     "110",
		Password:    "110",
		CountryCode: "86",
	}
	client := NewDBankHTTPClient("qwertyuiopqqqqqq", "1.0")
	aes := NewAesEncryption("qwertyuiopqqqqqq", "1ea58b048c5442ffff171d38f24bbdc5")

	encryptClient := NewEncryptionDBankRequestClient(client, aes)
	requestBody, err := encryptClient.
		BuildRequestBody("login/signin", loginRequest)
	if err != nil {
		t.Error(err)
	}


	body, err := encryptClient.Post("http://app.dbank123.com/api", requestBody)
	if err != nil {
		t.Error(err)
	}

	var data LoginResponse
	err = json.Unmarshal(body, &data)
	if err != nil {
		t.Error(err)
	}

	// TODO
}


func TestPost(t *testing.T){

	loginRequest := &LoginRequest{
		PointJson:   "233",
		Account:     "110",
		Password:    "110",
		CountryCode: "86",
	}
	client := NewDBankHTTPClient("qwertyuiopqqqqqq", "1.0")
	requestBody, err := client.
		BuildRequestBody("login/signin", loginRequest)
	if err != nil {
		t.Error(err)
	}


	body, err := client.Post("http://app.dbank123.com/api", requestBody)
	if err != nil {
		t.Error(err)
	}

	var data LoginResponse
	err = json.Unmarshal(body, &data)
	if err != nil {
		t.Error(err)
	}

	// TODO
}