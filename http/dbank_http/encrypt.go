package dbank_http

import "gitlab.com/mini-tech/aptoken/aplink-core/encrypt"

type Encryption interface {
	Encrypt(plainText []byte) ([]byte, error)
	Decrypt(cipherText []byte) ([]byte, error)
}

type AesEncryption struct {
	IV  string
	Key string
}

func NewAesEncryption(iv string, key string) *AesEncryption {
	return &AesEncryption{
		IV:  iv,
		Key: key,
	}
}

func (a *AesEncryption) Encrypt(plainText []byte) ([]byte, error) {
	return encrypt.EncryptCBC(plainText, []byte(a.Key), []byte(a.IV))
}

func (a *AesEncryption) Decrypt(cipherText []byte) ([]byte, error) {
	return encrypt.DecryptCBC(cipherText, []byte(a.Key), []byte(a.IV))
}
