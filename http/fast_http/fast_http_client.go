package fast_http

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.com/mini-tech/aptoken/aplink-core/log"
	"strings"
	"time"
)

type Client struct {
	client  *fasthttp.Request
	timeout time.Duration
}

const (
	defaultHTTPTimeout = 30 * time.Second
)

func NewHttpClient(timeout time.Duration) *Client {

	client := &Client{
		timeout: timeout,
	}
	if timeout == 0 {
		client.timeout = defaultHTTPTimeout
	}

	return client
}

// Get makes a HTTP GET request to provided URL
func (c *Client) Get(url string, headers map[string]string) (*fasthttp.Response, error) {
	var req = &fasthttp.Request{}
	req.SetRequestURI(url)
	req.Header.SetMethod("GET")

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return c.Do(req)
}

func (c *Client) GetWithArgs(args, headers map[string]string, url string) (*fasthttp.Response, error) {

	var req = &fasthttp.Request{}

	url = url + "?"
	for key, value := range args {
		url = url + key + "=" + value + "&"
	}

	url = strings.TrimRight(url, "&")
	req.SetRequestURI(url)
	req.Header.SetMethod("GET")

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return c.Do(req)
}

// Post makes a HTTP POST request to provided URL and requestBody
func (c *Client) Post(url string, body []byte, headers map[string]string) (*fasthttp.Response, error) {
	var req = &fasthttp.Request{}

	req.Header.SetMethod("POST")
	req.SetRequestURI(url)
	req.SetBody(body)

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return c.Do(req)
}

func (c *Client) PostWithArgs(url string, params map[string]interface{}, headers map[string]string) (*fasthttp.Response, error) {
	var req = &fasthttp.Request{}

	body, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req.Header.SetMethod("POST")
	req.SetRequestURI(url)
	req.SetBody(body)

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return c.Do(req)
}

func (c *Client) postWithBytes(url string, body []byte, headers map[string]string) (*fasthttp.Response, error) {
	var req = &fasthttp.Request{}

	req.Header.SetMethod("POST")
	req.SetRequestURI(url)
	req.SetBody(body)

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	return c.Do(req)
}

func (c *Client) PostJson(url string, body []byte, headers map[string]string) (*fasthttp.Response, error) {

	if headers == nil {
		headers = map[string]string{}
	}

	headers["Content-Type"] = "application/json"

	return c.postWithBytes(url, body, headers)
}

func (c *Client) PostFormUrlencoded(url string, params map[string]string, headers map[string]string) (*fasthttp.Response, error) {
	if headers == nil {
		headers = map[string]string{}
	}

	var req = &fasthttp.Request{}

	req.Header.SetMethod("POST")
	req.SetRequestURI(url)
	req.Header.SetContentType("application/x-www-form-urlencoded")

	for key, value := range headers {
		req.Header.Set(key, value)
	}

	log.Logger.Debugf("request url %v header %v params %v", url, params, req.Header.String())

	args := &fasthttp.Args{}
	for key, value := range params {
		args.Add(key, value)
	}

	b := req.Header.Cookie("au_token")
	if b != nil {
		log.Logger.Debugf(string(b))
	}

	if _, err := args.WriteTo(req.BodyWriter()); err != nil {
		return nil, err
	}

	return c.Do(req)
}

// Do makes an HTTP request with the native `http.Do` interface
func (c *Client) Do(req *fasthttp.Request) (*fasthttp.Response, error) {

	resp := &fasthttp.Response{}
	var err error
	err = fasthttp.DoTimeout(req, resp, c.timeout)
	return resp, err
}
