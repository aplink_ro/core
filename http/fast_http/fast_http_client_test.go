package fast_http

import (
	"fmt"
	"testing"
)

func TestClient_Get(t *testing.T) {
	client := NewHttpClient(0)
	resp, err := client.Get("https://www.baidu.com", nil)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(resp.Body()))
	fmt.Println(resp.StatusCode())
	fmt.Println(resp.RemoteAddr())
	fmt.Println(resp.LocalAddr())

}
