package log

const (
	ReqUrlKey         = "req_url"
	ReqParamKey       = "req_param"
	ReqMethodKey      = "req_method"
	ReqHeaderKey      = "req_header"
	ReqUidKey         = "req_uid"
	ReqIPKey          = "req_ip"
	RespHeaderKey     = "resp_header"
	RespKey           = "resp_result"
	ErrorCodeKey      = "error_code"
	HttpStatusCodeKey = "http_status"
	CostTime          = "cost_time"
)
