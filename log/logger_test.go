package log

import (
	"go.uber.org/zap"
	"testing"
	"time"
)

func Test_ZapLogger(t *testing.T) {
	//Init("debug", "test_service")
	t1 := time.Now()
	time.Sleep(time.Millisecond * 100)
	Logger.Infow("this is test info", ReqUrlKey, "https://www.baidu.com", ReqParamKey, 3, "backoff", time.Second, CostTime, time.Since(t1).Seconds())

}

func Test_SugaredLogger(t *testing.T) {
	//Init("info", "test_service")
	ZapLogger.Info("failed to fetch URL",
		// Structured context as strongly typed Field values.
		zap.String("url", "https://www.baidu.com"),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)
}
