package attack

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/mini-tech/aptoken/aplink-core/log"
)

const reg = `(.*?((SELECT)|(FROM)|(WHERE)|(UNION)|(SLEEP)|(DELETE)|(UPDATE)|(DROP)|(TRUNCATE)|(\?)|(ALTER)|(TABLE)|(INSERT)|(VALUES)).*?){2,}`

var Attack = &attack{}

type attack struct {
}

func (this *attack) Injection(ctx *http.Request) bool {
	getParams, check := this.getParams(ctx)
	if this.Verification(strings.ToUpper(check)) {
		log.Logger.Infof("URL[%v] PARAMS[%v]  check:[%s]", ctx.URL, getParams, check)
		return true
	}
	headParams, check := this.getHead(ctx)
	if this.Verification(strings.ToUpper(check)) {
		log.Logger.Infof("URL[%v]  PARAMS[%v]  check:[%s]", ctx.URL, headParams, check)
		return true
	}
	return false
}

func (this *attack) Verification(input string) bool {
	return len(regexp.MustCompile(reg).FindAllStringSubmatch(input, -1)) > 0
}

func (this *attack) getParams(ctx *http.Request) (string, string) {
	var lis []string
	var sp []string
	var params string
	if strings.EqualFold(ctx.Method, "GET") {
		if n := strings.SplitN(ctx.RequestURI, "?", 2); len(n) > 1 {
			params = n[1]
			n = strings.SplitN(n[1], "?", 2)
			if strings.Contains(n[0], "&") {
				sp = strings.Split(n[0], "&")
			} else {
				sp = append(sp, n[0])
			}
			for _, v := range sp {
				splitN := strings.SplitN(v, "=", 2)
				lis = append(lis, splitN[1])
			}
		}
		return params, strings.Join(lis, "")
	} else {
		if err := ctx.ParseForm(); err == nil {
			param := ctx.Form.Encode()
			if len(param) == 0 {
				buff, _ := ioutil.ReadAll(ctx.Body)
				buf := bytes.NewBuffer(buff)
				ctx.Body = ioutil.NopCloser(buf) // Write body back
				param = string(buff)
			} else {
				values, _ := url.ParseQuery(param)
				for key, value := range values {
					sp = append(sp, value[0])
					lis = append(lis, fmt.Sprintf("%v:%v", key, value))
				}
			}
		}
		return strings.Join(lis, " "), strings.Join(sp, "")
	}
}

func (this *attack) getHead(ctx *http.Request) (string, string) {
	headers := ctx.Header
	var sp []string
	var printSp []string
	for key, value := range headers {
		sp = append(sp, value[0])
		printSp = append(printSp, fmt.Sprintf("%v:%v", key, value))
	}
	return strings.Join(printSp, " "), strings.Join(sp, "")
}
