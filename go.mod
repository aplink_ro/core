module gitlab.com/mini-tech/aptoken/aplink-core

go 1.16

require (
	github.com/Shopify/sarama v1.29.1
	github.com/elazarl/goproxy v0.0.0-20210110162100-a92cc753f88e // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/goinggo/mapstructure v0.0.0-20140717182941-194205d9b4a9
	github.com/google/uuid v1.2.0
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.13.0 // indirect
	github.com/parnurzeal/gorequest v0.2.16
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/shopspring/decimal v1.2.0
	github.com/spf13/viper v1.8.1
	github.com/valyala/fasthttp v1.27.0
	go.uber.org/zap v1.18.1
	moul.io/http2curl v1.0.0 // indirect
)