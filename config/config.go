package config

import (
	"github.com/spf13/viper"
)

var ConfigManager = &configManager{}

type configManager struct {
	PublicViper *viper.Viper
	SecretViper *viper.Viper
}

func (this *configManager) Init(configFilePtr, secretFilePtr *string) {
	this.loadConfig(configFilePtr, secretFilePtr)
}

func (this *configManager) Parse(public interface{}, secret interface{}) error {
	if err := this.PublicViper.Unmarshal(&public); err != nil {
		return err
	}
	err := this.SecretViper.Unmarshal(&secret)
	return err
}

func (this *configManager) loadConfig(configFilePtr, secretFilePtr *string) {
	this.PublicViper = viper.New()
	this.SecretViper = viper.New()

	if configFilePtr == nil {
		this.PublicViper.SetConfigFile("./config/config.yaml")
	} else {
		this.PublicViper.SetConfigFile(*configFilePtr)
	}

	if secretFilePtr == nil {
		this.SecretViper.SetConfigFile("./secret/secret.yaml")
	} else {
		this.SecretViper.SetConfigFile(*secretFilePtr)
	}

	this.PublicViper.SetConfigType("yaml")
	if err := this.PublicViper.ReadInConfig(); err != nil {
	}

	this.SecretViper.SetConfigType("yaml")
	if err := this.SecretViper.ReadInConfig(); err != nil {
		panic(err)
	}

}
