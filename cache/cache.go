package cache

import (
	"github.com/patrickmn/go-cache"
	"time"
)

var CacheHelper *Cache

type Cache struct {
	cache *cache.Cache
}

func Init(expired_time time.Duration, cleanup_interval time.Duration) {
	CacheHelper = &Cache{}
	CacheHelper.cache = cache.New(expired_time, cleanup_interval)
}

func (c *Cache) Add(k string, x interface{}, d time.Duration) error {
	return c.cache.Add(k, x, d)
}

func (c *Cache) Set(k string, x interface{}, d time.Duration) {
	c.cache.Set(k, x, d)
}

func (c *Cache) Delete(k string) {
	c.cache.Delete(k)
}

func (c *Cache) Get(k string) (interface{}, bool) {
	return c.cache.Get(k)
}

func (c *Cache) Replace(k string, x interface{}, d time.Duration) error {
	return c.cache.Replace(k, x, d)
}
