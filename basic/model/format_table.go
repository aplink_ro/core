package model

type FormatTable struct {
	Headers  []FormatTableHeader `json:"headers"`
	Contents [][]interface{}     `json:"contents"`
}

type FormatTableHeader struct {
	Name         string            `json:"name"`
	Title        string            `json:"title"`
	EnumsMap     map[string]string `json:"enums_map,omitempty"`
	Type         string            `json:"type,omitempty"`
	RowSpanCount string            `json:"row_span_count,omitempty"`
}
